This program computes a GCL graph based on a user-written program in the *Guarded Command Language*.

The user-written program is read from a file, 'userProgram.txt.'.
The output graph is written to the file 'graph.gv'.
The final memory and program state is written to the file 'programOutput.txt'.
All the files can be found at '..\CSM_Mandatory\data\'.

To run the program simply run all lines in GCL.fsx in interactive mode. Future runs can be executed by simply running either the computeDet function or the interpret function.
The computeDet function takes one parameter (True -> Deterministic, False -> Non-Deterministic).