﻿// This script implements our interactive calculator

// We need to import a couple of modules, including the generated lexer and parser
#r @"refs/FsLexYacc.Runtime.dll"
#load "GCLTypesAST.fs"
#load "GCLParser.fs"
#load "GCLLexer.fs"

open Microsoft.FSharp.Text.Lexing
open System
open GCLTypesAST
open GCLParser
open GCLLexer
open System.IO

let path = Path.Combine(__SOURCE_DIRECTORY__, "data/")

// New nodes with increasing numbers
let mutable qi = 1
let qNew () =
  let result = qi
  qi <- qi+1
  "q" + (string) result

let rec toStringA e =
  match e with
    | Num(x) -> (string) x
    | Var(x) -> x
    | TimesExpr(x,y) -> checkA x + "*" + checkA y
    | DivExpr(x,y) -> checkA x + "/" + checkA  y
    | PlusExpr(x,y) -> checkA x + "+" + checkA  y
    | MinusExpr(x,y) -> checkA x + "-" + checkA  y
    | PowExpr(x,y) -> checkA x + "^" + checkA  y
    | UPlusExpr(x) -> checkA x
    | UMinusExpr(x) -> "-" + checkA x 
and checkA e =
    match e with
    | Num(_) -> toStringA e
    | Var(_) -> toStringA e
    | _ -> "(" + toStringA e + ")"
and toStringB e =
  match e with
    | Bool(x) -> (string) x
    | And(x,y) -> checkB x + "&" + checkB y
    | Or(x,y) -> checkB x + "|" + checkB y
    | SCAnd(x,y) -> checkB x + "&&" + checkB y
    | SCOr(x,y) -> checkB x + "||" + checkB y
    | Not(x) -> "!" + checkB x
    | Equals(x,y) -> checkA x + "=" + checkA y
    | NotEquals(x,y) -> checkA x + "!=" + checkA y
    | Greater(x,y) -> checkA x + ">" + checkA y
    | GreaterEquals(x,y) -> checkA x + ">=" + checkA y
    | Less(x,y) -> checkA x + "<" + checkA y
    | LessEquals(x,y) -> checkA x + "<=" + checkA y
and checkB e =
    match e with
    | Bool(_) -> toStringB e
    | _ -> "(" + toStringB e + ")"

let rec toStringCommand e = 
  match e with
    | Assign(x,y) -> x + ":=" + toStringA y
    | Skip -> "skip"
    | Seperator(x,y) -> toStringCommand x + ";" + toStringCommand y
    | _ -> "Something went wrong"
  
// Non-Deterministic Command and GuardedCommand funcions
let rec edgesC q1 q2 c =
  match c with
    | Assign(x,y) -> set [(q1, toStringCommand c, q2)]
    | Skip -> set [(q1, toStringCommand c, q2)]
    | Seperator(x,y) ->
        let qfresh = qNew()
        let E1 = edgesC q1 qfresh x
        let E2 = edgesC qfresh q2 y
        Set.union E1 E2
    | If(x) -> edgesGC q1 q2 x
    | Do(x) ->
        let b = _done x
        let E = edgesGC q1 q2 x
        Set.union E (set [(q1, toStringB b, q2)])
and edgesGC q1 q2 c =
  match c with
    | Eval(x, y) ->
        let qfresh = qNew()
        let E = edgesC qfresh q2 y
        Set.union E (set [(q1, toStringB x, qfresh)])
    | Choice(x,y) ->
        let E1 = edgesGC q1 q2 x
        let E2 = edgesGC q1 q2 y
        Set.union E1 E2
// Auxiliary function to handle loops
and _done x =
  match x with
    | Eval(a,b) -> Not(a)
    | Choice(a,b) -> And(_done(a), _done(b))
  
// Deterministic Command and GuardedCommand funcions
let rec det_edgesC q1 q2 c =
  match c with
    | Assign(x,y) -> set [(q1, toStringCommand c, q2)]
    | Skip -> set [(q1, toStringCommand c, q2)]
    | Seperator(x,y) ->
        let qfresh = qNew()
        Set.union (det_edgesC q1 qfresh x) (det_edgesC qfresh q2 y)
    | If(x) ->
        let (E,d) = det_edgesGC q1 q2 x (Bool false)
        E
    | Do(x) ->
        let (E,d) = det_edgesGC q1 q2 x (Bool false)
        Set.union E (set [(q1, toStringB (Not d), q2)])
// Takes the variable d contrary to the non-deterministic
and det_edgesGC q1 q2 c d =
  match c with
    | Eval(x, y) ->
        let qfresh = qNew()
        let E = det_edgesC qfresh q2 y
        (Set.union E (set [(q1, toStringB (And(x,Not(d))), qfresh)]), (Or(x,d)))
    | Choice(x,y) ->
        let (E1,d1) = det_edgesGC q1 q2 x d
        let (E2,d2) = det_edgesGC q1 q2 y d1
        (Set.union E1 E2, d2)

let parse input =
  // translate string into a buffer of characters
  let lexbuf = LexBuffer<char>.FromString input
  // translate the buffer into a stream of tokens and parse them
  let res = GCLParser.start GCLLexer.tokenize lexbuf
  // return the result of parsing (i.e. value of type "expr")
  res



let evalRes map b =
  Map.add "_eval" (if b then 1 else 0) map

let hasEval map =
  Map.find "_eval" map = 1

let stuck map =
  Map.add "_status" -1 map

let isStuck map =
  Map.find "_status" map = -1



let rec interpretA map e =
  let intA = interpretA map
  match e with
    | Num(x) -> x
    | Var(x) -> match Map.tryFind x map with
                | Some v -> v
                | None -> 0
    | TimesExpr(x,y) -> intA x * intA y
    | DivExpr(x,y) -> intA x / intA y
    | PlusExpr(x,y) -> intA x + intA y
    | MinusExpr(x,y) -> intA x - intA y
    | PowExpr(x,y) -> pown (intA x) (intA y)
    | UPlusExpr(x) -> intA x
    | UMinusExpr(x) -> - intA x

and interpretB map e =
  let intA = interpretA map
  let intB = interpretB map
  match e with
    | Bool(x) -> x
    | And(x,y) -> let b = intB y
                  intB x && b
    | Or(x,y) -> let b = intB y
                 intB x && b
    | SCAnd(x,y) -> intB x && intB y
    | SCOr(x,y) -> intB x || intB y
    | Not(x) -> not (intB x)
    | Equals(x,y) -> intA x = intA y
    | NotEquals(x,y) -> intA x <> intA y
    | Greater(x,y) -> intA x > intA y
    | GreaterEquals(x,y) -> intA x >= intA y
    | Less(x,y) -> intA x < intA y
    | LessEquals(x,y) -> intA x <= intA y
  
// Non-Deterministic Command and GuardedCommand funcions
let rec interpretC map c =
  let intA = interpretA map
  let intC = interpretC map
  let intGC = interpretGC map
  if isStuck map then map else
  match c with
    | Assign(x,y) -> Map.add x (intA y) map
    | Skip -> map
    | Seperator(x,y) -> interpretC (intC x) y
    | If(x) -> let map' = interpretGC map x
               if hasEval map' then map' else stuck map
    | Do(x) -> let map' = intGC x
               if hasEval map' then interpretC map' (Do x) else map

and interpretGC map c =
  let intB = interpretB map
  let intGC = interpretGC map
  match c with
    | Eval(x,y)  -> if (intB x) then interpretC (evalRes map true) y else (evalRes map false)
    | Choice(x,y) -> let map' = intGC x
                     if hasEval map' then map' else intGC y




// Prints the output to the file
let toFile s =
  let first = seq ["digraph program_graph {rankdir=LR;";
               "node [shape = circle]; q▷;";
               "node [shape = doublecircle]; q◀;";
               "node [shape = circle]"]
  //Appends the first four lines with the Sets
  let output = Set.fold (fun state (q1, c, q2) -> Seq.append state (seq [(q1 + " -> " + q2 + " [label = \"" + c + "\"];")])) first s
  System.IO.File.WriteAllText ((path + @"graph.gv"), String.concat "\n" output)


let rec computeDet det =
  try
    // We parse the input string
    let e = parse (System.IO.File.ReadAllText(path + @"userProgram.txt"))
    // Evaluates the 
    let s = if det then (det_edgesC "q▷" "q◀" e) 
            else (edgesC "q▷" "q◀" e)
    
    printfn "OK"
    toFile s
  with err -> printfn ("KO")

// -----  Interpreter  ----- //
let interpret () =
  try 
    let e = parse (System.IO.File.ReadAllText(path + @"userProgram.txt"))
    printfn "OK"
    let map = interpretC (Map.add "_status" 0 Map.empty) e
    let firstLine = "status:   " + if Map.find "_status" map <> -1 then "terminated" else "stuck"
    let map' = Map.remove "_eval" (Map.remove "_status" map)
    let output = Map.fold (fun out k v -> out + "\n" + k + ":   " + (string) v) firstLine map'
    System.IO.File.WriteAllText ((path + @"programOutput.txt"), output)
  with err -> printfn ("KO")

// Start interacting with the user (True => Deterministic, False => Non-Deterministic)
computeDet true
interpret ()