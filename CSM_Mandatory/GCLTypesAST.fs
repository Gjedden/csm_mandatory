﻿// This file implements a module where we define a data type "expr"
// to store represent arithmetic expressions
module GCLTypesAST

type aExpr =
  | Num of int
  | Var of (string)
  | TimesExpr of (aExpr * aExpr)
  | DivExpr of (aExpr * aExpr)
  | PlusExpr of (aExpr * aExpr)
  | MinusExpr of (aExpr * aExpr)
  | PowExpr of (aExpr * aExpr)
  | UPlusExpr of (aExpr)
  | UMinusExpr of (aExpr)
  
and bExpr =
  | Bool of (bool)
  | And of (bExpr * bExpr)
  | Or of (bExpr * bExpr)
  | SCAnd of (bExpr * bExpr)
  | SCOr of (bExpr * bExpr)
  | Not of (bExpr)
  | Equals of (aExpr * aExpr)
  | NotEquals of (aExpr * aExpr)
  | Greater of (aExpr * aExpr)
  | GreaterEquals of (aExpr * aExpr)
  | Less of (aExpr * aExpr)
  | LessEquals of (aExpr * aExpr)

and Command =
  | Assign of (string * aExpr)
  | Skip
  | Seperator of (Command * Command)
  | If of (GuardedCommand)
  | Do of (GuardedCommand)

and GuardedCommand =
  | Eval of (bExpr * Command)
  | Choice of (GuardedCommand * GuardedCommand)